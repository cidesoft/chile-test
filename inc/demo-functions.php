<?php
/**
 * demo functions.
 *
 * @package demo
 */

add_action( 'init', 'comunas_cpt_create' );

add_action( 'init', 'comunas_regiones_taxonomy', 0 );

add_action( 'init', 'terminos_taxonia_regiones', 0 );

define('data_regiones', array(
    array(
        'name' => 'La Araucanía',
        'slug' => 'araucania',
        'id'   => 'region-de-la-araucania'
    ),
    array(
        'name' => 'Arica y Parinacota',
        'slug' => 'arica',
        'id'   => 'region-de-arica-y-parinacota'
    ),
    array(
        'name' => 'Aysén del General Carlos Ibáñez del Campo',
        'slug' => 'aysen',
        'id'   => 'region-de-aysen'
    ),
    array(
        'name' => 'Los Lagos',
        'slug' => 'lagos',
        'id'   => 'region-de-los-lagos'
    ),
    array(
        'name' => 'Los Ríos',
        'slug' => 'rios',
        'id'   => 'region-de-los-rios'
    ),
    array(
        'name' => 'Santiago Metropolitan',
        'slug' => 'santiago',
        'id'   => 'santiago'
    ),
    array(
        'name' => 'Libertador General Bernardo OHiggins',
        'slug' => 'ohiggins',
        'id'   => 'region-del-libertador-bernardo-ohiggins'
    )
));

function comunas_cpt_create() {
	$labels = array(
		'name' => __( 'Comunas'), 
        'singular_name' => __( 'Comuna' ),
        'add_new' => _x( 'Añadir nueva', 'Comuna' ),
        'add_new_item' => __( 'Añadir nueva comuna'),
        'edit_item' => __( 'Editar comuna' ),
        'new_item' => __( 'Nueva comuna' ),
        'view_item' => __( 'Ver comuna' ),
		'featured_image'        => __( 'Imagen comuna', 'woocommerce' ),
        'search_items' => __( 'Buscar comunas' ),
        'not_found' =>  __( 'No se ha encontrado ningúna comuna' ),
        'not_found_in_trash' => __( 'No se han encontrado comunas en la papelera' ),
        'parent_item_colon' => ''
    );
 
    // Creamos un array para $args
    $args = array(
        'label' => __('Comunas'),
        'labels' => $labels,
        'public' => true,
        'can_export' => true,
        'show_ui' => true,
        '_builtin' => false,
        'capability_type' => 'post',        
        'hierarchical' => false,
        'rewrite' => array( "slug" => "Comunas" , 'with_front' => true),
        'supports'=> array('title','editor','thumbnail','excerpt') ,
        'show_in_nav_menus' => true,
        'taxonomies' => array( 'regiones'),
        'menu_icon' => 'dashicons-admin-appearance',
        'map_meta_cap' => true
        );
 
    register_post_type( 'comunas', $args ); /* Registramos y a funcionar */

    
}

function comunas_regiones_taxonomy() {
    $labels = array(
        'name' => _x( 'Regiones', 'taxonomy general name' ),
        'singular_name' => _x( 'Region', 'taxonomy singular name' ),
        'search_items' =>  __( 'Buscar Regiones' ),
        'popular_items' => __( 'Regiones Populares' ),
        'all_items' => __( 'Todas las Regiones' ),
        'parent_item' => null,
        'parent_item_colon' => null,
        'edit_item' => __( 'Editar Regiones' ),
        'update_item' => __( 'Actualizar Regiones' ),
        'add_new_item' => __( 'Añadir Regiones' ),
        'new_item_name' => __( 'Nuevo Nombre de Regiones' ),
        'separate_items_with_commas' => __( 'Separe las Regiones con comas' ),
        'add_or_remove_items' => __( 'Añadir o quitar Regiones' ),
        'choose_from_most_used' => __( 'Elija de las Regiones más usadas' ),
        );

    register_taxonomy('regiones','comunas', array(
        'labels' => $labels,
        'hierarchical' => true,
        'show_ui' => true,
        'query_var' => true,
        ));
}

function terminos_taxonia_regiones() {
    $parent_term = term_exists( 'regiones' ); // retorna una arreglo si declaramos una taxonomía
    $parent_term_id = $parent_term['term_id']; // tomamos el id del término (numerico)
    foreach(data_regiones as $key => $region){
        wp_insert_term(
            $region['name'], // el término
            'regiones', // la taxonomía
            array(
              'description'=> 'Region de ' . $region['name'],
              'slug' => $region['slug'],
              'parent'=> $parent_term_id
            )
          );
    }
   

}

/**
 * Apply inline style to the demo header.
 *
 * @uses  get_header_image()
 * @since  2.0.0
 */
function demo_header_styles() {
	$is_header_image = get_header_image();
	$header_bg_image = '';

	if ( $is_header_image ) {
		$header_bg_image = 'url(' . esc_url( $is_header_image ) . ')';
	}

	$styles = array();

	if ( '' !== $header_bg_image ) {
		$styles['background-image'] = $header_bg_image;
	}

	$styles = apply_filters( 'demo_header_styles', $styles );

	foreach ( $styles as $style => $value ) {
		echo esc_attr( $style . ': ' . $value . '; ' );
	}
}

function enqueue_styles_demo_front() {
	wp_enqueue_style( 'styles_demo_front_bootstrap', get_theme_file_uri('assets/css/bootstrap.min.css'), array(), '1.0.0'); 
	wp_enqueue_style( 'styles_demo_front', get_theme_file_uri('assets/css/front/style.css'), array(), '1.0.0'); 
	wp_enqueue_style( 'styles_demo_node', get_theme_file_uri('node_modules/reveal.js/dist/reveal.css'), array(), '1.0.0'); 
}

add_action( 'wp_enqueue_scripts', 'enqueue_styles_demo_front');

function enqueue_js_demo_front() {
	wp_enqueue_script( 'js_demo_front_bootstrap', get_theme_file_uri('assets/js/bootstrap.min.js'), array('jquery'), '1.0.0', true); 
	wp_enqueue_script( 'js_demo_front', get_theme_file_uri('assets/js/front/front.js'), array('jquery'), '1.0.0', true); 
    wp_enqueue_script( 'js_demo_node_rival', get_theme_file_uri('node_modules/reveal.js/dist/theme/black.css'), array(), '1.0.0', true); 
    
    wp_localize_script( 'js_demo_front', 'data_comunas', array(
        'data' => data_regiones ,
        'ajax_url' => admin_url('admin-ajax.php')
    ));

}

add_action( 'wp_enqueue_scripts', 'enqueue_js_demo_front');

function demo_get_post() {

    $args = array(
        'post_type' => 'comunas',
	    'post_status' => 'publish',
	    'orderby' => 'date',
	    'order'   => 'DESC',
	    'tax_query' => array(
		    array(
		        'taxonomy' => 'regiones', // Nombre de la taxonomía creada
		        'field' => 'name', // Como pasaremos el parámetro, en este caso como el slug
		        'terms' => $_POST['name'], // NAME de la taxonomía
		    )
	    )
	);
	$posts = null;	
    $posts = new WP_Query($args);
    $post = array();
    if( $posts->have_posts() )
    {
        while ($posts->have_posts())
		{
            $posts->the_post();
            $thumb_id = get_post_thumbnail_id();
			$thumb_url = wp_get_attachment_image_src($thumb_id,'medium', true);
            $url_imagen = $thumb_url[0];
            
            $post[] =  array(
                'title' => get_the_title(),
                'fecha' => get_the_date(),
                'enlace' => get_permalink(),
                'img' => $url_imagen,
                'descrip' => get_the_excerpt()
            );
		}
        wp_send_json(['status' => true, 'data' => $post]);
    }else{
        wp_send_json(['status' => false]);

    }
	wp_reset_query();
    wp_die();
}

add_action( 'wp_ajax_demo_get_post', 'demo_get_post');
