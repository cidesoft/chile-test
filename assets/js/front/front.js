jQuery(document).ready(function($) {
    data_comunas.data.forEach((value) => {
        $("#" + value['id']).click((e) => {
            $.ajax({
                type: 'POST',
                url: data_comunas.ajax_url,
                data: { action: 'demo_get_post', name: value['name'] },
                success: function(datos) {
                    if (datos.status) {
                        alert(datos.data[0].title + "\n" + datos.data[0].descrip);
                    }

                },
                error: function(error) {
                    console.log(error)
                }
            });

        });

    });

});