<?php
/**
 * demo engine room
 *
 * @package demo
 */

/**
 * Assign the demo version to a var
 */
$theme              = wp_get_theme( 'demo' );
$demo_version = $theme['Version'];

require 'inc/demo-functions.php';
